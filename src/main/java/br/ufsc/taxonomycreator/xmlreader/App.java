package br.ufsc.taxonomycreator.xmlreader;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Entity;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Hello world!
 *
 */
public class App {
	
	private static final String FILENAME = "/curriculo.xml";

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.parse(App.class.getResourceAsStream(FILENAME));
		
		NodeList list = document.getChildNodes();
		listNodes(list);
		
	}

	private static void listNodes(NodeList list) {
		if (list.getLength() > 0) {
			for (int index = 0; index < list.getLength(); index++) {
				Node node = list.item(index);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					
					StringBuilder builder = new StringBuilder();
					builder.append(node.getNodeName() + " // ");

					NamedNodeMap map = node.getAttributes();
					
					for (int mapIndex = 0; mapIndex < map.getLength(); mapIndex++) {
						Node attribute = map.item(mapIndex);
						builder.append(attribute.getNodeName() + " : " + attribute.getNodeValue() + " "); 
					}

					System.out.println(builder.toString());
					listNodes(node.getChildNodes());
					
				}
			}
		}
	}
}
